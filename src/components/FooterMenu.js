import React from 'react';
import {
   graphql,
   StaticQuery,
   Link
} from 'gatsby';
import styled from 'styled-components';

const FooterMenuWrapper = styled.div `
   background-color: rgba(255, 180, 180);
   display: flex;
   margin: 0;
`

const MenuItem = styled(Link) `
   color: white;
   display: block;
   padding: 12px 16px;
`

const MainMenu = () => (<StaticQuery query = {graphql `
{
      allWordpressWpApiMenusMenusItems(filter: {
         name: {
            eq: "Footer menu"
         }
      }) {
         edges {
            node {
               name
               items {
                  title
                  object_slug
               }
            }
         }
      }
   }
`
   }
   render = {
      props => ( <FooterMenuWrapper> {
            props.allWordpressWpApiMenusMenusItems.edges[0].node.items.map(item => (
               <MenuItem to={`/${item.object_slug}`} key={item.title}>
                  {item.title}
               </MenuItem>
            ))
         } </FooterMenuWrapper>
      )
   }
   />
);

export default MainMenu
