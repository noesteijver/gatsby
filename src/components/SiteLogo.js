import React from 'react';
import { graphql, StaticQuery } from 'gatsby';
import styled from 'styled-components';

const SiteLogo = styled.img `
   width: auto;
   height: 100%;
   max-height: 50px;
   margin: 5px 10px 5px 0;
`


const Logo = () => (
   <StaticQuery query={graphql`{
      allWordpressWpLogo {
         edges {
            node {
               url {
                  source_url
               }
            }
         }
      }
   }`
   } render = { props => (
      <SiteLogo src={props.allWordpressWpLogo.edges[0].node.url.source_url} />
   )} />
);

export default Logo